use rocket;
use rocket::config::{Config, Environment};
use rocket::{get, routes};
use rocket_contrib::json::Json;

use crate::data::Data;
use crate::host::Host;
use crate::hostfile;
use ipnet::Ipv4Net;
use rayon::prelude::*;
use reqwest::header;
use reqwest::Client;
use rocket::State;
use std::net::{IpAddr, Ipv4Addr};
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;

#[get("/")]
fn index(data: State<Arc<Mutex<Data>>>) -> Json<Host> {
    let data = data.lock().expect("Couldn't unlock mutex");
    Json(data.host.clone())
}

#[get("/peers")]
fn peers(data: State<Arc<Mutex<Data>>>) -> Json<Vec<Host>> {
    let data = data.lock().expect("Couldn't unlock mutex");
    Json(data.peers.clone())
}

pub fn start(data: Arc<Mutex<Data>>) -> Result<(), rocket::config::ConfigError> {
    let config = Config::build(Environment::Staging)
        .address("0.0.0.0")
        .finalize()?;
    rocket::custom(config)
        .manage(data)
        .mount("/", routes![index, peers])
        .launch();

    Ok(())
}

pub fn get_host() -> reqwest::Result<Host> {
    let ip = IpAddr::V4(Ipv4Addr::LOCALHOST);
    let uri = ip_to_addr(&ip, 8000, "/");
    Ok(reqwest::get(&uri)?.json()?)
}

pub fn get_peers() -> reqwest::Result<Vec<Host>> {
    let ip = IpAddr::V4(Ipv4Addr::LOCALHOST);
    let uri = ip_to_addr(&ip, 8000, "/peers");
    Ok(reqwest::get(&uri)?.json()?)
}

fn ip_to_addr(ip: &IpAddr, port: u32, route: &str) -> String {
    format!("http://{}:{}{}", ip, port, route)
}

fn create_client() -> Client {
    let headers = header::HeaderMap::new();

    reqwest::Client::builder()
        .default_headers(headers)
        .timeout(Duration::from_millis(250))
        .build()
        .expect("Couldn't create client")
}

pub fn watch_ips(range: String, data: Arc<Mutex<Data>>) {
    loop {
        let peers = scan_ip_range(&range);
        for peer in &peers {
            hostfile::add_host(peer);
        }

        {
            let mut data = data.lock().expect("Couldn't unlock mutex");
            data.peers = peers;
        }

        thread::sleep(Duration::from_secs(60));
    }
}

fn scan_ip_range(range: &str) -> Vec<Host> {
    let range: Ipv4Net = range.parse().expect("Couldn't parse ip range");
    let hosts: Vec<_> = range.hosts().collect();

    let hosts = hosts.par_iter().filter_map(query_host);

    hosts.collect()
}

fn query_host(address: &Ipv4Addr) -> Option<Host> {
    let uri = format!("http://{}:8000", address);
    let response = create_client().get(&uri).send().ok();
    match response?.json() {
        Ok(host) => {
            let mut host: Host = host;
            host.address = Some(IpAddr::V4(address.clone()));
            host.is_this_machine = false;
            Some(host)
        }
        Err(e) => {
            println!("{:?}", e);
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_scan_ip_range() {
        let hosts = scan_ip_range("172.16.17.0/24".into());

        dbg!(hosts);
    }
}
