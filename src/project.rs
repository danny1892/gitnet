use git2::{Commit, ObjectType, Repository};
use std::env;
use std::path::PathBuf;
use walkdir::DirEntry;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Project {
    pub name: String,
    /// Location of project in the file system
    pub location: PathBuf,
    pub branches: Vec<Branch>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Branch {
    pub name: String,
    pub latest_commit: i64,
    pub commit_count: usize,
    pub committer: String,
}

impl Project {
    pub fn new(name: String) -> Project {
        Project {
            name,
            location: env::current_dir().unwrap(),
            branches: vec![Branch {
                name: "master".into(),
                latest_commit: 0,
                commit_count: 1,
                committer: "Unknown".to_string(),
            }],
        }
    }

    pub fn from_dir_entry(dir: DirEntry) -> Option<Project> {
        let name = dir
            .clone()
            .into_path()
            .parent()?
            .file_name()?
            .to_str()
            .unwrap_or("Directory has invalid name")
            .to_string();

        Some(Project {
            name,
            location: dir.clone().into_path(),
            branches: vec![],
        })
    }

    pub fn get_repo_status(project: &Project) -> Result<Vec<Branch>, git2::Error> {
        let repo = Repository::open(&project.location)?;
        let branches = Repository::branches(&repo, Some(git2::BranchType::Local))?;
        let mut branch_list = Vec::new();
        for branch in branches {
            let temp_branch = branch?;
            let name = temp_branch.0.name()?.expect("fuck").to_string();
            let cur_commit = temp_branch
                .0
                .into_reference()
                .peel(ObjectType::Commit)?
                .into_commit()
                .unwrap();
            let time = cur_commit.time().seconds();
            let commit_count = count_commits(&repo, &cur_commit);
            let author = cur_commit.committer().to_string();
            branch_list.push(Branch {
                name,
                latest_commit: time,
                commit_count: commit_count,
                committer: author,
            });
        }
        Ok(branch_list)
    }
}

pub fn count_commits(repo: &Repository, commit: &Commit) -> usize {
    let mut revwalk = match repo.revwalk() {
        Ok(rw) => rw,
        Err(e) => return 0,
    };
    revwalk.push(commit.id());

    revwalk.count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_repo_status() {
        let project = Project::new("test".into());
        let branches = Project::get_repo_status(&project);

        dbg!(project);
        dbg!(&branches);

        assert!(branches.is_ok());
    }
}
