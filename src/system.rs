use std::net::IpAddr;

pub fn normalise_to_hostname(name: String) -> String {
    name.chars()
        // Remove spaces
        .filter(|c| *c != ' ')
        .collect()
}

pub fn is_ipv4(ip: &IpAddr) -> bool {
    match ip {
        IpAddr::V4(_) => true,
        _ => false,
    }
}

pub fn get_ip() -> Option<IpAddr> {
    let interfaces = pnet::datalink::interfaces();

    let docker = interfaces
        .clone()
        .into_iter()
        .filter(|i| i.name == "docker0")
        .next();
    let first = interfaces.first();

    let interface = if docker.is_some() {
        docker.unwrap()
    } else {
        first.expect("No first interface").clone()
    };

    let ips = interface.ips;
    let ipv4 = ips
        .into_iter()
        .map(|ip| ip.ip())
        .filter(is_ipv4)
        .collect::<Vec<IpAddr>>();

    ipv4.first().map(|ip| ip.clone())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_normalise_removes_spaces() {
        assert_eq!("testtest", normalise_to_hostname("test test".into()))
    }

    #[test]
    fn tets_get_ip() {
        let ip = get_ip();

        dbg!(ip);
    }
}
